declare module '*.png';
declare module '*.jpg';
declare module '*.svg';
declare module '*.webp';
declare module '*.webm';
declare module '*.mp4';
declare module '*.module.scss';

declare interface PageMeta {
  title: string;
  slug: string;
  shortTitle?: string;
  order?: string;
  description?: string;
}
