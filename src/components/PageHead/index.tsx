import React, { FC } from 'react';
import config, { siteUrl } from '../../../gatsby-config';
import { Literata } from './LiterataFontStyles';
import { Geologica } from './GeologicaFontStyles';
import globalOgImage from './og-image.png';

type Props = {
  title?: string;
  description?: string;
  themeColor?: string;
  og?: {
    image?: string;
    title?: string;
    description?: string;
  };
};

export const PageHead: FC<Props> = (props) => {
  const pageTitleSegments = [props.title];

  // Makes titles to be shorter
  if (
    !props.title ||
    (!props.title.includes('Програм') && props.title.length < 50)
  ) {
    pageTitleSegments.push(config.siteMetadata?.title as string);
  }

  const pageTitle = pageTitleSegments.filter(Boolean).join(' — ');
  const defaultDescription =
    'Програма розвитку велосипедного транспорту на території Львівської МТГ до 2033 року';

  const { og } = props;
  const ogImage = (og && og.image) ?? `${siteUrl}${globalOgImage}`;

  return (
    <>
      <title>{pageTitle}</title>
      <meta
        name="description"
        content={props.description ?? defaultDescription}
      />
      {props.themeColor && (
        <meta name="theme-color" content={props.themeColor} />
      )}
      <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="" />
      <Geologica />
      <Literata />
      <meta
        name="google-site-verification"
        content="Em3pvS2oir_VRIwFl6Dy6diCTX93DYa6ptGMf0OeGOE"
      />
      <meta name="msvalidate.01" content="A39043D876008F938E62826FAD6CE0C2" />
      <meta property="og:type" content="website" />
      <meta property="og:locale" content="uk_UA" />
      <meta property="og:title" content={props.og?.title || props.title} />
      <meta
        property="og:description"
        content={
          props.og?.description || props.description || defaultDescription
        }
      />
      <meta
        property="og:site_name"
        content="Львівська Асоціація Велосипедистів"
      />
      {ogImage && <meta property="og:image" content={ogImage} />}
    </>
  );
};
