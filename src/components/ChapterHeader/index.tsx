import React, { FC } from 'react';
import * as classes from './ChapterHeader.module.scss';

type Props = {
  title: string;
  order?: string;
};

export const ChapterHeader: FC<Props> = (props) => {
  const { order, title } = props;

  return (
    <div className={classes.host}>
      <div className={['container', classes.inner].join(' ')}>
        <h1 className={classes.title}>
          {order && <span className={classes.digit}>{order}</span>} {title}
        </h1>
      </div>
    </div>
  );
};
