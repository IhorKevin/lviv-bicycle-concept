import React, { FC } from 'react';
import { Link } from 'gatsby';
import { MdWest as PrevIcon } from 'react-icons/md';
import { MdEast as NextIcon } from 'react-icons/md';
import * as classes from './Pagination.module.scss';

type NavItem = {
  title: string;
  pathname: string;
};

type Props = {
  prev?: NavItem;
  next?: NavItem;
};

export const Pagination: FC<Props> = (props) => {
  const { prev, next } = props;

  const hostClasses = [classes.host];
  if (!prev || !next) hostClasses.push(classes.single);
  const hostCN = hostClasses.join(' ');

  return (
    <div className={hostCN}>
      {prev && (
        <Link to={prev.pathname} className={classes.navItem}>
          <div className={classes.label}>
            <PrevIcon aria-hidden /> Попередня
          </div>
          {prev.title}
        </Link>
      )}
      {next && (
        <Link to={next.pathname} className={classes.navItem}>
          <div className={classes.label}>
            Наступна <NextIcon aria-hidden />
          </div>
          {next.title}
        </Link>
      )}
    </div>
  );
};
