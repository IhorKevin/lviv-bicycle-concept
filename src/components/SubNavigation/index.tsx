import React, { FC, ReactNode } from 'react';
import { Link } from 'gatsby';
import * as classes from './SubNavigation.module.scss';

type Props = {
  className?: string;
  items: {
    href: string;
    label: ReactNode;
  }[];
};

export const SubNavigation: FC<Props> = (props) => {
  const hostCN = [classes.host, props.className].join(' ');

  return (
    <nav className={hostCN}>
      <ul className={classes.list}>
        {props.items.map((item) => (
          <li key={item.href} className={classes.item}>
            <Link to={item.href} className={classes.link}>
              {item.label}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};
