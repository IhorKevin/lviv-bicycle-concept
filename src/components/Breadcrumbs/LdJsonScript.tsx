import React, { FC } from 'react';

type Props = {
  schema: Record<string, any> | Record<string, any>[];
};

export const LdJsonScript: FC<Props> = (props) => {
  const { schema } = props;

  return (
    <script
      type="application/ld+json"
      dangerouslySetInnerHTML={{ __html: JSON.stringify(schema) }}
    />
  );
};
