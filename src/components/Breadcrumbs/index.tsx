import React, { FC, ReactElement } from 'react';
import { renderToString } from 'react-dom/server';
import { Link } from 'gatsby';
import { siteUrl } from '../../../gatsby-config';
import { BreadcrumbsSchema } from './BreadcrumbsSchema';
import * as classes from './Breadcrumbs.module.scss';

type Segment = {
  name: string | ReactElement;
  url?: string;
};

type SegmentItemProps = Segment & {
  last?: boolean;
};

type Props = {
  className?: string;
  path: Segment[];
};

const SegmentItem: FC<SegmentItemProps> = (props) => {
  return (
    <li className={classes.item}>
      {props.url && (
        <Link
          className={classes.link}
          to={props.url}
          aria-current={props.last ? 'page' : undefined}
        >
          {props.name}
        </Link>
      )}
      {!props.url && props.name}
    </li>
  );
};

export const Breadcrumbs: FC<Props> = (props) => {
  const hostCN = [classes.breadcrumbs, props.className].join(' ');

  return (
    <nav className={hostCN} aria-label="Breadcrumb">
      <ol className={classes.path}>
        <SegmentItem name="Головна" url="/" />
        {props.path.map((segment, index) => (
          <SegmentItem
            {...segment}
            key={`segment-${index}`}
            last={props.path.length === index + 1}
          />
        ))}
      </ol>
      <BreadcrumbsSchema
        items={props.path.map((item) => {
          const url = item.url ? siteUrl + item.url : '';
          return {
            name:
              typeof item.name === 'string'
                ? item.name
                : renderToString(item.name),
            url,
          };
        })}
      />
    </nav>
  );
};
