import React, { FC } from 'react';
import { LdJsonScript } from './LdJsonScript';

type ListItem = {
  name: string;
  url: string;
};

type Props = {
  items?: ListItem[];
};

export const BreadcrumbsSchema: FC<Props> = (props) => {
  const { items } = props;
  if (!items || !items.length) return null;

  const list = items.map((item, index) => ({
    '@type': 'ListItem',
    position: index + 1,
    name: item.name,
    item: item.url,
  }));

  const result = {
    '@context': 'https://schema.org',
    '@type': 'BreadcrumbList',
    itemListElement: list,
  };
  return <LdJsonScript schema={result} />;
};
