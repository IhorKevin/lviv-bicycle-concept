import React, { FC, useState, lazy, PropsWithChildren, useEffect } from 'react';
import * as classes from './RouteMap.module.scss';

const LazyMap = lazy(() => {
  return import('./Map');
});

export const ClientSide: FC<PropsWithChildren<{ className?: string }>> = (
  props,
) => {
  const [ready, setReady] = useState<boolean>(false);

  useEffect(() => {
    setReady(true);
  }, []);

  if (ready) return <>{props.children}</>;

  return <div className={[classes.mapContainer, classes.placeholder].join(' ')}>Карта велодоріжок Львів</div>;
};

type BikeLayer = {
  pathname: string;
  color: string;
  name: string;
  outer?: boolean;
};

type Props = {
  layers: BikeLayer[];
};

export const RouteMap: FC<Props> = (props) => {
  const { layers } = props;
  const layerNames = layers.map((l) => l.name);
  const initialSet = new Set(layerNames);
  const [enabledLayers, setEnabledLayers] = useState<Set<string>>(initialSet);

  return (
    <div>
      <ClientSide>
        <LazyMap
          layers={layers}
          isLayerVisible={(name) => enabledLayers.has(name)}
        />
      </ClientSide>

      <div className={classes.legend}>
        {props.layers.map((layer) => {
          return (
            <div key={layer.name}>
              <label>
                <input
                  type="checkbox"
                  name="layers"
                  value={layer.name}
                  checked={enabledLayers.has(layer.name)}
                  onChange={(event) => {
                    const { checked } = event.currentTarget;
                    const { name } = layer;

                    if (checked) enabledLayers.add(name);
                    else enabledLayers.delete(name);

                    setEnabledLayers(new Set(enabledLayers));
                  }}
                />{' '}
                <svg
                  viewBox="0 0 48 4.8"
                  className={classes.indicator}
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <line
                    stroke={layer.color}
                    strokeWidth="6"
                    x1="0"
                    y1="2.4"
                    x2="48"
                    y2="2.4"
                    strokeDasharray={layer.outer ? '6 4' : undefined}
                  />
                </svg>{' '}
                {layer.name}
              </label>
            </div>
          );
        })}
      </div>
    </div>
  );
};
