import React, { FC, useEffect, useState } from 'react';
import 'leaflet/dist/leaflet.css';
import * as classes from './RouteMap.module.scss';

const {
  GeoJSON,
  LayersControl,
  MapContainer,
  TileLayer,
  useMapEvents,
  useMap,
} = await import('react-leaflet');

const tileLayer = {
  url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
};

const tileLayerStadia = {
  url: 'https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png',
  attribution:
    '&copy; <a href="https://www.stadiamaps.com/" target="_blank">Stadia Maps</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
};

const tileLayerCarto = {
  url: 'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png',
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
};

const initialZoom = 11;
const initialCenter = {
  lat: 49.8738074,
  lng: 24.0287432,
};
const initialLineWeight = 2;

type BikeLayer = {
  pathname: string;
  color: string;
  name: string;
  outer?: boolean;
};

type RouteLayerProps = {
  name: string;
  pathname: string;
  color: string;
  visible?: boolean;
  dashed?: boolean;
};

const RouteLayer: FC<RouteLayerProps> = (props) => {
  const { pathname } = props;
  const [data, setData] = useState<any>(undefined);
  const [lineWeight, setLineWeight] = useState(initialLineWeight);
  const map = useMapEvents({
    zoom: () => {
      if (map.getZoom() > 14) setLineWeight(3);
      else setLineWeight(initialLineWeight);
    },
  });

  const loadLayer = async (url: string) => {
    const response = await fetch(url);
    return await response.json();
  };

  useEffect(() => {
    loadLayer(pathname).then((data) => setData(data));
  }, [pathname]);

  if (!data || !props.visible) return null;

  return (
    <GeoJSON
      key={props.name}
      data={data}
      style={{
        color: props.color,
        dashArray: props.dashed ? '4 6' : undefined,
        weight: lineWeight,
      }}
    ></GeoJSON>
  );
};

const MetaKeyListener: FC = () => {
  const map = useMap();

  useEffect(() => {
    const metaKeyListener = (event: KeyboardEvent) => {
      event.metaKey
        ? map.scrollWheelZoom.enable()
        : map.scrollWheelZoom.disable();
    };

    window.document.addEventListener('keydown', metaKeyListener);
    window.document.addEventListener('keyup', metaKeyListener);

    return () => {
      window.document.removeEventListener('keydown', metaKeyListener);
      window.document.removeEventListener('keyup', metaKeyListener);
    };
  }, []);

  return null;
};

type Props = {
  layers: BikeLayer[];
  isLayerVisible: (name: string) => boolean;
};

const Map: FC<Props> = (props) => {
  const { layers } = props;
  const { lat, lng } = initialCenter;

  return (
    <MapContainer
      className={classes.mapContainer}
      center={[lat, lng]}
      zoom={initialZoom}
      scrollWheelZoom={false}
    >
      {layers.map((layer) => {
        const { pathname, color, name } = layer;

        return (
          <RouteLayer
            key={name}
            name={name}
            pathname={pathname}
            color={color}
            visible={props.isLayerVisible(name)}
            dashed={layer.outer}
          />
        );
      })}
      <LayersControl>
        <LayersControl.BaseLayer name="OSM">
          <TileLayer
            attribution={tileLayer.attribution}
            url={tileLayer.url}
            maxZoom={20}
          />
        </LayersControl.BaseLayer>
        <LayersControl.BaseLayer name="Stadia">
          <TileLayer
            attribution={tileLayerStadia.attribution}
            url={tileLayerStadia.url}
            maxZoom={20}
          />
        </LayersControl.BaseLayer>
        <LayersControl.BaseLayer name="Carto" checked>
          <TileLayer
            attribution={tileLayerCarto.attribution}
            url={tileLayerCarto.url}
            maxZoom={20}
          />
        </LayersControl.BaseLayer>
      </LayersControl>
      <MetaKeyListener />
    </MapContainer>
  );
};

export default Map;
