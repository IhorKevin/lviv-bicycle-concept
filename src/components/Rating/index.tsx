import React, { FC } from 'react';
import { MdOutlinePedalBike as ComfortIcon } from 'react-icons/md';
import { MdOutlineShield as SafetyIcon } from 'react-icons/md';
import * as classes from './Rating.module.scss';

type Props = {
  value: number;
  type: 'safety' | 'comfort';
};

const percentageToHue = (percentage: number): number => {
  const startHue = -50; // less than red
  const endHue = 120; // more than green
  const range = endHue - startHue;
  const value = (percentage * range) / 100;
  return Math.round(startHue + value);
};

export const Rating: FC<Props> = (props) => {
  const { value } = props;
  const percentage = value * 20;
  const color = `hsl(${percentageToHue(percentage)}, 95%, 35%)`;
  const label = props.type === 'safety' ? 'Безпека' : 'Комфорт';
  const Icon = props.type === 'safety' ? SafetyIcon : ComfortIcon;

  return (
    <div className={classes.host} title={label}>
      <Icon className={classes.icon} aria-hidden />
      <div
        className={classes.track}
        role="progressbar"
        aria-label={label}
        aria-valuemin={1}
        aria-valuemax={5}
        aria-valuenow={value}
      >
        <span
          className={classes.progress}
          style={{
            width: `${percentage}%`,
            backgroundColor: color,
          }}
        ></span>
        <span className={classes.cell} />
        <span className={classes.cell} />
        <span className={classes.cell} />
        <span className={classes.cell} />
      </div>
      <span className="caption">{value.toFixed(2)}</span>
    </div>
  );
};
