import { meta as vstupMeta } from '../../pages/vstup/_meta';
import { meta as meta1 } from '../../pages/1-obgruntuvannia-potreby-stvorennia-programy/_meta';
import { meta as meta2 } from '../../pages/2-analityka/_meta';
import { meta as meta3 } from '../../pages/3-meta-programy/_meta';
import { meta as meta4 } from '../../pages/4-viziia/_meta';
import { meta as meta5 } from '../../pages/5-tsili-programy/_meta';
import { meta as meta6 } from '../../pages/6-pryntsypy-rozvytku-velomerezhi-ta-veloinfrastruktury/_meta';
import { meta as meta7 } from '../../pages/7-velomerezha-ta-velosypedna-infrastruktura-lvivskoi-mth/_meta';
import { meta as meta8 } from '../../pages/8-zavdannia-programy/_meta';
import { meta as meta9 } from '../../pages/9-vprovadzhennia-programy/_meta';
import { meta as meta10 } from '../../pages/10-finansove-zabezpechennia-vykonannia-programy/_meta';
import { meta as meta11 } from '../../pages/11-faktory-ta-ryzyky-shcho-mozhut-vplyvaty-na-realizatsiiu-programy/_meta';
import { meta as meta12 } from '../../pages/12-vykonannia-ta-monitorynh-realizatsii-programy/_meta';
import { meta as meta13 } from '../../pages/13-ochikuvani-rezultaty-vid-realizatsii-zavdan-programy-u-2033-rotsi/_meta';
import { meta as dodatkyMeta } from '../../pages/dodatky/_meta';

const buildLabel = (meta: PageMeta): string => {
  return `${meta.order} ${meta.shortTitle || meta.title}`;
};

export const menuItems = [
  {
    slug: vstupMeta.slug,
    label: vstupMeta.title,
  },
  {
    slug: meta1.slug,
    label: buildLabel(meta1),
  },
  {
    slug: meta2.slug,
    label: buildLabel(meta2),
  },
  {
    slug: meta3.slug,
    label: buildLabel(meta3),
  },
  {
    slug: meta4.slug,
    label: buildLabel(meta4),
  },
  {
    slug: meta5.slug,
    label: buildLabel(meta5),
  },
  {
    slug: meta6.slug,
    label: buildLabel(meta6),
  },
  {
    slug: meta7.slug,
    label: buildLabel(meta7),
  },
  {
    slug: meta8.slug,
    label: buildLabel(meta8),
  },
  {
    slug: meta9.slug,
    label: buildLabel(meta9),
  },
  {
    slug: meta10.slug,
    label: buildLabel(meta10),
  },
  {
    slug: meta11.slug,
    label: buildLabel(meta11),
  },
  {
    slug: meta12.slug,
    label: buildLabel(meta12),
  },
  {
    slug: meta13.slug,
    label: buildLabel(meta13),
  },
  {
    slug: dodatkyMeta.slug,
    label: dodatkyMeta.title,
  },
];
