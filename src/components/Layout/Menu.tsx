import React, { FC, useEffect, useRef } from 'react';
import { Link } from 'gatsby';
import { MdArrowForward as CloseIcon } from 'react-icons/md';
import { useClickOutside } from './click-outside';
import { menuItems } from './data';
import * as classes from './Menu.module.scss';

type Props = {
  open: boolean;
  onClose: () => void;
};

export const Menu: FC<Props> = (props) => {
  const { open, onClose } = props;
  const containerRef = useRef<HTMLDivElement | null>(null);

  useClickOutside(containerRef, () => onClose());

  const hostClasses = [classes.container];
  if (open) hostClasses.push(classes.open);
  const hostCN = hostClasses.join(' ');

  useEffect(() => {
    const keyHandler = (event: KeyboardEvent) => {
      if (event.key === 'Escape') onClose();
    };
    window.addEventListener('keyup', keyHandler);

    return () => {
      window.removeEventListener('keyup', keyHandler);
    };
  }, []);

  return (
    <div className={hostCN} ref={containerRef}>
      <nav className={classes.body}>
        <ul className={classes.list}>
          {menuItems.map((item) => (
            <li key={item.slug}>
              <Link
                to={`/${item.slug}`}
                className={classes.link}
                activeClassName={classes.active}
                tabIndex={open ? undefined : -1}
              >
                {item.label}
              </Link>
            </li>
          ))}
        </ul>
        <button type="button" className={classes.close} onClick={onClose}>
          Закрити <CloseIcon aria-hidden />
        </button>
      </nav>
    </div>
  );
};
