import React, { FC, PropsWithChildren, useState } from 'react';
import { Link } from 'gatsby';
import { Menu } from './Menu';
import * as classes from './Layout.module.scss';

type Props = {
  alternativeHeader?: boolean;
};

export const Layout: FC<PropsWithChildren<Props>> = (props) => {
  const [menuOpen, setMenuOpen] = useState<boolean>(false);
  const headerCN = [classes.header];
  if (props.alternativeHeader) headerCN.push(classes.inverse);

  return (
    <div className={classes.root}>
      <header className={headerCN.join(' ')}>
        <Link to="/" className={classes.logo}>
          Велопрограма 2033
        </Link>
        <button
          type="button"
          className={classes.button}
          onClick={() => setMenuOpen(true)}
        >
          <span className={classes.burger}>
            <span className={classes.line}></span>
            <span className={classes.line}></span>
          </span>{' '}
          Меню
        </button>
      </header>
      <Menu open={menuOpen} onClose={() => setMenuOpen(false)} />
      <main className={classes.content}>{props.children}</main>
      <footer className={classes.footer}>
        <div>
          <h2>Про проєкт</h2>
          <p className="caption">
            Цей сайт є неофіційною версією документа "Програма розвитку
            велосипедного транспорту на території Львівської міської
            територіальної громади до 2033 року". Ухвала{' '}
            <a
              href="https://www8.city-adm.lviv.ua/inteam/uhvaly.nsf/(SearchForWeb)/6F6977884146D7D4C2258A6000298FB1?OpenDocument"
              target="_blank"
              rel="noreferrer"
            >
              №3983
            </a>{' '}
            від <time dateTime="2023-11-02">2 листопада 2023 року</time>.
          </p>
          <p className="caption">
            Мета — зручність читання на електронних пристроях. Відтак можуть
            бути незначні відмінності у форматуванні, пунктуації тощо, водночас
            зміст повністю збережений і відповідає офіційному документу.
          </p>
        </div>
        <div>
          <h2>Помічне</h2>
          <ul className={classes.list}>
            <li>
              <Link to="/slovnyk">Словник</Link>
            </li>
            <li>
              <Link to="/dodatky">Додатки</Link>
            </li>
            <li>
              <Link to="/passport">Паспорт</Link>
            </li>
          </ul>
        </div>
        <div>
          <h2>Контакти</h2>
          <p className="caption">
            Сайт адмініструє{' '}
            <a href="https://lav.org.ua" target="_blank">
              ГО "Львівська асоціація велосипедистів"
            </a>
            . Зауваження та пропозиції надсилати на:
          </p>
          <ul className={classes.list}>
            <li>
              <a href="mailto:info@lav.org.ua" target="_blank">
                info@lav.org.ua
              </a>
            </li>
            <li>
              <a href="https://lav.org.ua/about-lav" target="_blank">
                Сторінки у соцмережах
              </a>
            </li>
          </ul>
        </div>
      </footer>
    </div>
  );
};
