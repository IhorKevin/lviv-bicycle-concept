import { MutableRefObject, useEffect } from 'react';

export const isClickOutside = (
  element: HTMLElement,
  target: EventTarget,
): boolean => {
  return target instanceof Node && !element.contains(target);
};

export const useClickOutside = (
  ref: MutableRefObject<HTMLElement | null>,
  onClickOutside: () => void,
) => {
  useEffect(() => {
    const handleClickOutside = ({ target }: MouseEvent) => {
      if (ref.current && target && isClickOutside(ref.current, target)) {
        onClickOutside();
      }
    };
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  }, [onClickOutside, ref]);
};
