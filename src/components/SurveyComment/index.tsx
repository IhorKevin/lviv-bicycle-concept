import React, { FC, PropsWithChildren } from 'react';
import * as classes from './SurveyComment.module.scss';
import avatar1 from './avatar-male.svg';
import avatar2 from './avatar-female.svg';

type Props = {
  author: string;
  sex: 'male' | 'female';
};

export const SurveyComment: FC<PropsWithChildren<Props>> = (props) => {

  const icon = props.sex === 'male' ? avatar1 : avatar2;

  return (
    <figure className={classes.host}>
      <figcaption className={classes.author}>{props.author}</figcaption>
      <blockquote className={classes.body}>{props.children}</blockquote>
      <img className={classes.avatar} src={icon} alt="" />
    </figure>
  );
};
