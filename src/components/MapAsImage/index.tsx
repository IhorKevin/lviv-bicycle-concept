import React, { FC } from 'react';
import { MdOpenInNew } from 'react-icons/md';
import * as classes from './MapAsImage.module.scss';

type Props = {
  href: string;
};

export const MapAsImage: FC<Props> = (props) => {
  return (
    <a href={props.href} className={classes.link} target="_blank">
      Переглянути у вигляді зображення <MdOpenInNew />
    </a>
  );
};
