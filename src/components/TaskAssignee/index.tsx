import React, { FC, ReactNode } from 'react';
import * as classes from './TaskAssignee.module.scss';

type Props = {
  name: ReactNode;
  deadline?: string;
  multi?: boolean;
};

export const TaskAssignee: FC<Props> = (props) => {
  return (
    <div className={classes.host}>
      <div>
        <span className={classes.assignee}>
          {props.multi ? 'Виконавці' : 'Виконавець'}
        </span>
        : {props.name}
      </div>
      {props.deadline && (
        <div>
          <span className={classes.assignee}>Термін</span>: {props.deadline}
        </div>
      )}
    </div>
  );
};
