import React, { FC, useEffect, useRef } from 'react';
import {
  Chart,
  LinearScale,
  BarController,
  CategoryScale,
  BarElement,
  Legend,
} from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import * as classes from './MobilityChart.module.scss';

Chart.register(
  LinearScale,
  BarController,
  BarElement,
  CategoryScale,
  Legend,
  ChartDataLabels,
);

type Props = {};

export const MobilityChart: FC<Props> = (props) => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null);

  const dataAsPercentage = (row: (number | null)[]) => {
    return row.map((value) => {
      if (value === null) return null;
      return value / 100;
    });
  };

  useEffect(() => {
    const isLargeScreen = window.outerWidth >= 1024;
    const style = getComputedStyle(window.document.body);

    Chart.defaults.font.family = style.getPropertyValue(
      '--sans-serif-font-family',
    );
    Chart.defaults.color = style.getPropertyValue('--body-color');
    Chart.defaults.borderColor = style.getPropertyValue('--border-color');

    if (canvasRef.current) {
      const barChart = new Chart(canvasRef.current, {
        type: 'bar',
        data: {
          labels: [
            'Інше',
            'Скутер, мопед тощо',
            'Таксі',
            'Велосипед',
            'Ходіння пішки',
            'Автомобіль',
            'Громадський транспорт',
          ],
          datasets: [
            {
              label: 'Теплий період: 2019',
              backgroundColor: '#128760',
              data: dataAsPercentage([1, null, 1, 6, 18, 23, 52]),
              minBarLength: 5,
            },
            {
              label: 'Холодний період: 2021',
              backgroundColor: '#0a3840',
              data: dataAsPercentage([0.1, 1, 1, 4, 24, 25, 46]),
              minBarLength: 5,
            },
            {
              label: 'Теплий період: 2021',
              backgroundColor: '#E27508',
              data: dataAsPercentage([0, 0.3, 0.1, 8, 24, 22, 46]),
              minBarLength: 5,
            },
          ],
        },
        options: {
          maintainAspectRatio: false,
          scales: {
            y: {
              ticks: {
                format: {
                  style: 'percent',
                },
              },
            },
          },
          plugins: {
            datalabels: {
              anchor: 'end',
              align: 'top',
              rotation: isLargeScreen ? 0 : -90,
              formatter: (value, context) => {
                if (value === null) return 'Немає даних';
                const percentage = value * 100;
                return percentage.toFixed(1) + '%';
              },
            },
          },
        },
      });
    }
  }, []);

  return (
    <div className="fluid-container">
      <h2>Як ви переважно здійснюєте переміщення містом?</h2>
      <div className={classes.chart}>
        <canvas ref={canvasRef}></canvas>
      </div>
    </div>
  );
};
