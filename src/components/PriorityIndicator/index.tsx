import React, { FC } from 'react';

import * as classes from './PriorityIndicator.module.scss';

type Props = {
  value: 1 | 2 | 3 | 4;
};

export const PriorityIndicator: FC<Props> = (props) => {
  const hostCN = [classes.host, classes[`priority${props.value}`]].join(' ');
  return <span className={hostCN}></span>;
};
