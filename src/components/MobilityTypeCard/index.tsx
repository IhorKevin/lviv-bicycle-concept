import React, { FC } from 'react';
import { Rating } from '../Rating';
import * as classes from './MobilityTypeCard.module.scss';

type Props = {
  imageSlug: string;
  title: string;
  summary?: string;
  safety: number;
  comfort: number;
};

// small    336px
// middle   672px
// large    790px
// huge     1280px

const sizes = [336, 672, 790, 1280] as const;

export const MobilityTypeCard: FC<Props> = (props) => {
  const { imageSlug } = props;

  const folder = '/mobility-types/';
  const src = `${folder}${imageSlug}.jpg`;
  const sources = sizes.map(
    (size) => `${folder}${imageSlug}-${size}.webp ${size}w`,
  );
  const srcSet = sources.join(',');

  return (
    <div className={classes.host}>
      <picture>
        <source
          srcSet={srcSet}
          type="image/webp"
          sizes="calc(100vw - 2 * var(--page-padding)), (min-width: 1024px) 336px"
        />
        <img
          src={src}
          className={classes.image}
          alt=""
          width="100%"
          height="200"
          loading="lazy"
        />
      </picture>
      <div>
        <div className={classes.title}>{props.title}</div>
        {props.summary && <p className={classes.summary}>{props.summary}</p>}
        <div>
          <ul className={classes.list}>
            <li>
              <Rating value={props.safety} type="safety" />
            </li>
            <li>
              <Rating value={props.comfort} type="comfort" />
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};
