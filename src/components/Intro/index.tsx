import React, { FC, PropsWithChildren } from 'react';
import * as classes from './Intro.module.scss';
import videoWebm from './lviv-bicycle-concept.webm';
import videoMp4 from './lviv-bicycle-concept.mp4';
import posterJpg from './poster.jpg';

type Props = {};

export const Intro: FC<PropsWithChildren> = (props) => {
  return (
    <div className={classes.intro}>
      <video className={classes.video} autoPlay loop muted playsInline controls={false} poster={posterJpg}>
        <source src={videoWebm} type="video/webm" />
        <source src={videoMp4} type="video/mp4" />
      </video>
      <div className={classes.overlay}></div>
      <div className={classes.slot}>{props.children}</div>
    </div>
  );
};
