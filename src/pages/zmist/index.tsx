import React, { FC } from 'react';
import { HeadFC, Link } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { meta } from './_meta';
import { meta as vstupMeta } from '../vstup/_meta';
import { meta as meta1 } from '../1-obgruntuvannia-potreby-stvorennia-programy/_meta';
import { meta as meta1Child1 } from '../1-1-vyklyky-shcho-stoyat-na-shliahu-realizatsii-programy/_meta';
import { meta as meta1Child2 } from '../1-2-zviazok-programy-z-inshymy-stratehichnymy-dokumentamy-mista/_meta';
import { meta as meta2 } from '../2-analityka/_meta';
import { meta as meta2Child1 } from '../2-1-osnovni-rezultaty-doslidzhennia/_meta';
import { meta as meta2Child2 } from '../2-2-otsinka-umov-peresuvannia-na-velosypedi/_meta';
import { meta as meta3 } from '../3-meta-programy/_meta';
import { meta as meta4 } from '../4-viziia/_meta';
import { meta as meta5 } from '../5-tsili-programy/_meta';
import { meta as meta6 } from '../6-pryntsypy-rozvytku-velomerezhi-ta-veloinfrastruktury/_meta';
import { meta as meta7 } from '../7-velomerezha-ta-velosypedna-infrastruktura-lvivskoi-mth/_meta';
import { meta as meta7Child1 } from '../7-1-velomerezha-lvivskoi-mth/_meta';
import { meta as meta7Child2 } from '../7-2-velosypedna-infrastruktura/_meta';
import { meta as meta7Child3 } from '../7-3-tymchasovi-zakhody-z-rozvytku-velomerezhi/_meta';
import { meta as meta8 } from '../8-zavdannia-programy/_meta';
import { meta as meta8Child1 } from '../8-1-stratehichni-ta-rehuliatorni-zavdannia/_meta';
import { meta as meta8Child2 } from '../8-2-instytutsiini-zavdannia/_meta';
import { meta as meta8Child3 } from '../8-3-infrastrukturni-zavdannia/_meta';
import { meta as meta8Child4 } from '../8-4-inshi-zavdannia/_meta';
import { meta as meta9 } from '../9-vprovadzhennia-programy/_meta';
import { meta as meta10 } from '../10-finansove-zabezpechennia-vykonannia-programy/_meta';
import { meta as meta11 } from '../11-faktory-ta-ryzyky-shcho-mozhut-vplyvaty-na-realizatsiiu-programy/_meta';
import { meta as meta12 } from '../12-vykonannia-ta-monitorynh-realizatsii-programy/_meta';
import { meta as meta13 } from '../13-ochikuvani-rezultaty-vid-realizatsii-zavdan-programy-u-2033-rotsi/_meta';
import { meta as dodatkyMeta } from '../dodatky/_meta';
import { meta as dodatkyMeta1 } from '../dodatky/1-perspektyvna-velosypedna-merezha-lvivskoi-mth-typy-marshrutiv/_meta';
import { meta as dodatkyMeta2 } from '../dodatky/2-plan-dii-z-budivnytstva-ta-oblashtuvannia-velosypednoi-merezhi-lvivskoi-mth/_meta';
import { meta as dodatkyMeta3 } from '../dodatky/3-perspektyvna-velosypedna-merezha-lvivskoi-mth-priorytetnist/_meta';
import { meta as dodatkyMeta4 } from '../dodatky/4-naiavna-velosypedna-merezha-lvivskoi-mth/_meta';
import { meta as slovnykMeta } from '../slovnyk/_meta';
import { meta as passportMeta } from '../passport/_meta';
import * as classes from './Page.module.scss';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout>
      <div className="container section">
        <h1>{title}</h1>
        <ul className={classes.list}>
          <li>
            <Link to={`/${vstupMeta.slug}`}>{vstupMeta.title}</Link>
          </li>
          <li>
            <Link to={`/${meta1.slug}`}>
              {meta1.order} {meta1.title}
            </Link>
            <ol>
              <li>
                <Link to={`/${meta1Child1.slug}`}>
                  {meta1Child1.order} {meta1Child1.title}
                </Link>
              </li>
              <li>
                <Link to={`/${meta1Child2.slug}`}>
                  {meta1Child2.order} {meta1Child2.title}
                </Link>
              </li>
            </ol>
          </li>
          <li>
            <Link to={`/${meta2.slug}`}>
              {meta2.order} {meta2.title}
            </Link>
            <ol>
              <li>
                <Link to={`/${meta2Child1.slug}`}>
                  {meta2Child1.order} {meta2Child1.title}
                </Link>
              </li>
              <li>
                <Link to={`/${meta2Child2.slug}`}>
                  {meta2Child2.order} {meta2Child2.title}
                </Link>
              </li>
            </ol>
          </li>
          <li>
            <Link to={`/${meta3.slug}`}>
              {meta3.order} {meta3.title}
            </Link>
          </li>
          <li>
            <Link to={`/${meta4.slug}`}>
              {meta4.order} {meta4.title}
            </Link>
          </li>
          <li>
            <Link to={`/${meta5.slug}`}>
              {meta5.order} {meta5.title}
            </Link>
          </li>
          <li>
            <Link to={`/${meta6.slug}`}>
              {meta6.order} {meta6.title}
            </Link>
          </li>
          <li>
            <Link to={`/${meta7.slug}`}>
              {meta7.order} {meta7.title}
            </Link>
            <ol>
              <li>
                <Link to={`/${meta7Child1.slug}`}>
                  {meta7Child1.order} {meta7Child1.title}
                </Link>
              </li>
              <li>
                <Link to={`/${meta7Child2.slug}`}>
                  {meta7Child2.order} {meta7Child2.title}
                </Link>
              </li>
              <li>
                <Link to={`/${meta7Child3.slug}`}>
                  {meta7Child3.order} {meta7Child3.title}
                </Link>
              </li>
            </ol>
          </li>
          <li>
            <Link to={`/${meta8.slug}`}>
              {meta8.order} {meta8.title}
            </Link>
            <ol>
              <li>
                <Link to={`/${meta8Child1.slug}`}>
                  {meta8Child1.order} {meta8Child1.title}
                </Link>
              </li>
              <li>
                <Link to={`/${meta8Child2.slug}`}>
                  {meta8Child2.order} {meta8Child2.title}
                </Link>
              </li>
              <li>
                <Link to={`/${meta8Child3.slug}`}>
                  {meta8Child3.order} {meta8Child3.title}
                </Link>
              </li>
              <li>
                <Link to={`/${meta8Child4.slug}`}>
                  {meta8Child4.order} {meta8Child4.title}
                </Link>
              </li>
            </ol>
          </li>
          <li>
            <Link to={`/${meta9.slug}`}>
              {meta9.order} {meta9.title}
            </Link>
          </li>
          <li>
            <Link to={`/${meta10.slug}`}>
              {meta10.order} {meta10.title}
            </Link>
          </li>
          <li>
            <Link to={`/${meta11.slug}`}>
              {meta11.order} {meta11.title}
            </Link>
          </li>
          <li>
            <Link to={`/${meta12.slug}`}>
              {meta12.order} {meta12.title}
            </Link>
          </li>
          <li>
            <Link to={`/${meta13.slug}`}>
              {meta13.order} {meta13.title}
            </Link>
          </li>
          <li>
            <Link to={`/${dodatkyMeta.slug}`}>{dodatkyMeta.title}</Link>
            <ol>
              <li>
                <Link to={`/${dodatkyMeta.slug}/${dodatkyMeta1.slug}`}>
                  {dodatkyMeta1.shortTitle}. {dodatkyMeta1.title}
                </Link>
              </li>
              <li>
                <Link to={`/${dodatkyMeta.slug}/${dodatkyMeta2.slug}`}>
                  {dodatkyMeta2.shortTitle}. {dodatkyMeta2.title}
                </Link>
              </li>
              <li>
                <Link to={`/${dodatkyMeta.slug}/${dodatkyMeta3.slug}`}>
                  {dodatkyMeta3.shortTitle}. {dodatkyMeta3.title}
                </Link>
              </li>
              <li>
                <Link to={`/${dodatkyMeta.slug}/${dodatkyMeta4.slug}`}>
                  {dodatkyMeta4.shortTitle}. {dodatkyMeta4.title}
                </Link>
              </li>
            </ol>
          </li>
          <li>
            <Link to={`/${slovnykMeta.slug}`}>{slovnykMeta.title}</Link>
          </li>
          <li>
            <Link to={`/${passportMeta.slug}`}>{passportMeta.title}</Link>
          </li>
        </ul>
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <PageHead title={title} />;

export default Page;
