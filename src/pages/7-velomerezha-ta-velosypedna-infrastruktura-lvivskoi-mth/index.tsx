import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { ChapterHeader } from '../../components/ChapterHeader';
import { SubNavigation } from '../../components/SubNavigation';
import { Pagination } from '../../components/Pagination';
import { meta } from './_meta';
import { meta as prevMeta } from '../6-pryntsypy-rozvytku-velomerezhi-ta-veloinfrastruktury/_meta';
import { meta as meta1 } from '../7-1-velomerezha-lvivskoi-mth/_meta';
import { meta as meta2 } from '../7-2-velosypedna-infrastruktura/_meta';
import { meta as meta3 } from '../7-3-tymchasovi-zakhody-z-rozvytku-velomerezhi/_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout alternativeHeader>
      <ChapterHeader title={title} order={meta.order} />
      <div className="container">
        <SubNavigation
          items={[
            {
              href: `/${meta1.slug}`,
              label: meta1.title,
            },
            {
              href: `/${meta2.slug}`,
              label: meta2.title,
            },
            {
              href: `/${meta3.slug}`,
              label: meta3.title,
            },
          ]}
        />
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: meta1.title,
            pathname: `/${meta1.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <PageHead title={title} themeColor="#cc3333" />;

export default Page;
