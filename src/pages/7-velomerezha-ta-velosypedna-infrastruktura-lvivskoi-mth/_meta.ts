export const meta: PageMeta = {
  slug: '7-velomerezha-ta-velosypedna-infrastruktura-lvivskoi-mth',
  title: 'Веломережа та велосипедна інфраструктура на території Львівської МТГ',
  shortTitle: 'Веломережа та велосипедна інфраструктура',
  order: '7',
};
