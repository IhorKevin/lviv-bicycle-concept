export const meta: PageMeta = {
  slug: '2-3-budivnytstvo-novoi-velosypednoi-merezhi',
  title: 'ІІІ-ій пріоритет: Будівництво нової велосипедної мережі',
  shortTitle: 'ІІІ-ій пріоритет',
  order: '2.3',
};
