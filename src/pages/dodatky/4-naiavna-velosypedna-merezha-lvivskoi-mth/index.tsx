import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import ogPreview from './og-preview.jpg';
import { siteUrl } from '../../../../gatsby-config';
import { Layout } from '../../../components/Layout';
import { PageHead } from '../../../components/PageHead';
import { Breadcrumbs } from '../../../components/Breadcrumbs';
import { MapAsImage } from '../../../components/MapAsImage';
import { RouteMap } from '../../../components/RouteMap';
import { meta } from './_meta';
import { meta as chapterMeta } from '../_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout>
      <div className="fluid-container section">
        <Breadcrumbs
          path={[
            {
              name: chapterMeta.title,
              url: `/${chapterMeta.slug}`,
            },
            {
              name: meta.shortTitle || meta.title,
            },
          ]}
        />
        <h1>{title}</h1>
        <RouteMap
          layers={[
            {
              name: 'Наявна велосипедна інфраструктура',
              pathname: '/geojson/available-bicycle-network.json',
              color: '#1F00B3',
            },
          ]}
        />
        <MapAsImage href="/maps/2023_Наявна_велосипедна_мережа_Львівської_МТГ-А3.jpg" />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead
    title={title}
    og={{ image: `${siteUrl}${ogPreview}` }}
    description="Карта велодоріжок Львів, наявні"
  />
);
export default Page;
