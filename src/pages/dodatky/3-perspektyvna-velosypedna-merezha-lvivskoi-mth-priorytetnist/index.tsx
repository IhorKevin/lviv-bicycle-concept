import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import ogPreview from './og-preview.jpg';
import { siteUrl } from '../../../../gatsby-config';
import { Layout } from '../../../components/Layout';
import { PageHead } from '../../../components/PageHead';
import { Breadcrumbs } from '../../../components/Breadcrumbs';
import { MapAsImage } from '../../../components/MapAsImage';
import { RouteMap } from '../../../components/RouteMap';
import { meta } from './_meta';
import { meta as chapterMeta } from '../_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout>
      <div className="fluid-container section">
        <Breadcrumbs
          path={[
            {
              name: chapterMeta.title,
              url: `/${chapterMeta.slug}`,
            },
            {
              name: meta.shortTitle || meta.title,
            },
          ]}
        />
        <h1>{title}</h1>
        <RouteMap
          layers={[
            {
              name: 'Наявна велосипедна інфраструктура',
              pathname: '/geojson/available-bicycle-network.json',
              color: '#000000',
            },
            {
              name: 'I-ий пріоритет',
              pathname: '/geojson/priorities/priority-1_07.12.2023.json',
              color: 'var(--priority-1-color)',
            },
            {
              name: 'II-ий пріоритет',
              pathname: '/geojson/priorities/priority-2_07.12.2023.json',
              color: 'var(--priority-2-color)',
            },
            {
              name: 'III-ий пріоритет',
              pathname: '/geojson/priorities/priority-3_07.12.2023.json',
              color: 'var(--priority-3-color)',
            },
            {
              name: 'IV-ий пріоритет',
              pathname: '/geojson/priorities/priority-4_07.12.2023.json',
              color: 'var(--priority-4-color)',
            },
          ]}
        />
        <MapAsImage href="/maps/2023_Перспективна_велосипедна_мережа_Львівської_МТГ-пріоритети-А3.jpeg" />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead
    title={title}
    og={{ image: `${siteUrl}${ogPreview}` }}
    description="Карта веломаршрутів Львів, пріоритети"
  />
);
export default Page;
