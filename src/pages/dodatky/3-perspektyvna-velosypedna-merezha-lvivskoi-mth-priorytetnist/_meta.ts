export const meta: PageMeta = {
  slug: '3-perspektyvna-velosypedna-merezha-lvivskoi-mth-priorytetnist',
  title: 'Перспективна велосипедна мережа Львівської МТГ. Пріоритетність будівництва та облаштування',
  shortTitle: 'Додаток 3',
  order: '3',
};
