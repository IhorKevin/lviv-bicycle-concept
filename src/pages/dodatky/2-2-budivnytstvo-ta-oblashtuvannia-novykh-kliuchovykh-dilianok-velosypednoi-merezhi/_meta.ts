export const meta: PageMeta = {
  slug: '2-2-budivnytstvo-ta-oblashtuvannia-novykh-kliuchovykh-dilianok-velosypednoi-merezhi',
  title:
    'ІІ-ий пріоритет: Будівництво та облаштування нових ключових ділянок велосипедної мережі',
  shortTitle: 'ІІ-ий пріоритет',
  order: '2.2',
};
