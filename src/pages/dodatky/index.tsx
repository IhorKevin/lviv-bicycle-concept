import React, { FC } from 'react';
import { HeadFC, Link } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { meta } from './_meta';
import { meta as meta1 } from './1-perspektyvna-velosypedna-merezha-lvivskoi-mth-typy-marshrutiv/_meta';
import { meta as meta2 } from './2-plan-dii-z-budivnytstva-ta-oblashtuvannia-velosypednoi-merezhi-lvivskoi-mth/_meta';
import { meta as meta3 } from './3-perspektyvna-velosypedna-merezha-lvivskoi-mth-priorytetnist/_meta';
import { meta as meta4 } from './4-naiavna-velosypedna-merezha-lvivskoi-mth/_meta';

const title = meta.title;
const navItems = [
  {
    slug: meta1.slug,
    title: meta1.title,
  },
  {
    slug: meta2.slug,
    title: meta2.title,
  },
  {
    slug: meta3.slug,
    title: meta3.title,
  },
  {
    slug: meta4.slug,
    title: meta4.title,
  },
] as const;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout>
      <div className="container section">
        <h1>{title}</h1>
        <nav>
          <ol>
            {navItems.map((item) => (
              <li key={item.slug}>
                <Link to={`/dodatky/${item.slug}`}>{item.title}</Link>
              </li>
            ))}
          </ol>
        </nav>
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <PageHead title={title} />;

export default Page;
