import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import ogPreview from './og-preview.jpg';
import { siteUrl } from '../../../../gatsby-config';
import { Layout } from '../../../components/Layout';
import { PageHead } from '../../../components/PageHead';
import { Breadcrumbs } from '../../../components/Breadcrumbs';
import { MapAsImage } from '../../../components/MapAsImage';
import { RouteMap } from '../../../components/RouteMap';
import { meta } from './_meta';
import { meta as chapterMeta } from '../_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout>
      <div className="fluid-container section">
        <Breadcrumbs
          path={[
            {
              name: chapterMeta.title,
              url: `/${chapterMeta.slug}`,
            },
            {
              name: meta.shortTitle || meta.title,
            },
          ]}
        />
        <h1>{title}</h1>
        <RouteMap
          layers={[
            {
              name: 'Міські магістральні',
              pathname: '/geojson/route-types/trunk_07.12.2023.json',
              color: '#B72A3B',
            },
            {
              name: "Міські під'їздні",
              pathname: '/geojson/route-types/access_07.12.2023.json',
              color: '#DCA129',
            },
            {
              name: 'Альтернативні і рекреаційні',
              pathname: '/geojson/route-types/alternative_07.12.2023.json',
              color: '#3A8A35',
            },
            {
              name: 'Приміські',
              pathname: '/geojson/route-types/suburban_07.12.2023.json',
              color: '#932A93',
            },
            {
              name: 'Приміські не в межах громади',
              pathname: '/geojson/route-types/suburban-outside_07.12.2023.json',
              color: '#932A93',
              outer: true,
            },
          ]}
        />
        <MapAsImage href="/maps/2023_Перспективна_велосипедна_мережа_Львівської_МТГ-типи_маршрутів-А3.jpeg" />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead
    title={title}
    og={{ image: `${siteUrl}${ogPreview}` }}
    description="Карта велодоріжок Львів, майбутні"
  />
);

export default Page;
