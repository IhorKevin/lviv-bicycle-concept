export const meta: PageMeta = {
  slug: '2-1-ziednannia-naiavnoi-velosypednoi-merezhi',
  title: 'І-ий пріоритет: З’єднання наявної велосипедної мережі',
  shortTitle: 'І-ий пріоритет',
  order: '2.1',
};
