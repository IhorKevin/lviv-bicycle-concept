export const meta: PageMeta = {
  slug: '2-plan-dii-z-budivnytstva-ta-oblashtuvannia-velosypednoi-merezhi-lvivskoi-mth',
  title:
    'План дій з будівництва та облаштування велосипедної мережі Львівської МТГ',
  shortTitle: 'Додаток 2',
  order: '2',
};
