import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { Layout } from '../../../components/Layout';
import { PageHead } from '../../../components/PageHead';
import { SubNavigation } from '../../../components/SubNavigation';
import { Breadcrumbs } from '../../../components/Breadcrumbs';
import { PriorityIndicator } from '../../../components/PriorityIndicator';
import { meta } from './_meta';
import { meta as chapterMeta } from '../_meta';
import { meta as meta1 } from '../2-1-ziednannia-naiavnoi-velosypednoi-merezhi/_meta';
import { meta as meta2 } from '../2-2-budivnytstvo-ta-oblashtuvannia-novykh-kliuchovykh-dilianok-velosypednoi-merezhi/_meta';
import { meta as meta3 } from '../2-3-budivnytstvo-novoi-velosypednoi-merezhi/_meta';
import { meta as meta4 } from '../2-4-budivnytstvo-inshykh-dilianok-velosypednoi-merezhi/_meta';

const title = meta.title;
const navItems = [
  {
    href: `/${chapterMeta.slug}/${meta1.slug}`,
    label: (
      <span>
        <PriorityIndicator value={1} /> <br /> {meta1.title}
      </span>
    ),
  },
  {
    href: `/${chapterMeta.slug}/${meta2.slug}`,
    label: (
      <span>
        <PriorityIndicator value={2} /> <br /> {meta2.title}
      </span>
    ),
  },
  {
    href: `/${chapterMeta.slug}/${meta3.slug}`,
    label: (
      <span>
        <PriorityIndicator value={3} /> <br /> {meta3.title}
      </span>
    ),
  },
  {
    href: `/${chapterMeta.slug}/${meta4.slug}`,
    label: (
      <span>
        <PriorityIndicator value={4} /> <br /> {meta4.title}
      </span>
    ),
  },
];

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout>
      <div className="container section">
        <Breadcrumbs
          path={[
            {
              name: chapterMeta.title,
              url: `/${chapterMeta.slug}`,
            },
            {
              name: meta.shortTitle || title,
            },
          ]}
        />
        <h1>{title}</h1>
        <SubNavigation items={navItems} />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <PageHead title={title} />;

export default Page;
