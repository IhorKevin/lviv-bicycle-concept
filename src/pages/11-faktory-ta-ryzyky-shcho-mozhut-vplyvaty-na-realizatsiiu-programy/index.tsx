import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { Pagination } from '../../components/Pagination';
import { ChapterHeader } from '../../components/ChapterHeader';
import { meta } from './_meta';
import { meta as prevMeta } from '../10-finansove-zabezpechennia-vykonannia-programy/_meta';
import { meta as nextMeta } from '../12-vykonannia-ta-monitorynh-realizatsii-programy/_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout alternativeHeader>
      <ChapterHeader title={title} order={meta.order} />
      <div className="container">
        <p>
          На реалізацію Програми можуть впливати різноманітні фактори та ризики.
          Їхній вплив може бути як позитивний та сприяти реалізації Програми,
          так і негативний, що навпаки буде сповільнювати впровадження, або й
          взагалі зупинить її реалізацію.
        </p>
        <p>
          Робоча група з розробки Програми проаналізувала можливі фактори та
          ризики та подала їх, згрупувавши у формі{' '}
          <abbr
            lang="en"
            title="political, economic, socio-cultural, technological, legal and environmental"
          >
            PESTLE
          </abbr>
          -аналізу.
        </p>
      </div>

      <section className="container">
        <h2>11.1 Політичні</h2>
        <p>Потенційні ризики:</p>
        <ul>
          <li>війна з російською федерацією;</li>
          <li>
            місцеві вибори (зміна політичних сил у міській раді може вплинути на
            реалізацію Програми).
          </li>
        </ul>
        <p>Потенційні позитивні фактори:</p>
        <ul>
          <li>світові тенденції з розвитку велотранспорту;</li>
          <li>
            статус України як кандидата на вступ до Європейського Союзу та
            прийняття європейського законодавства, яке передбачає розвиток
            велотранспорту;
          </li>
          <li>
            наявність державної політики, спрямованої на розвиток велосипедної
            інфраструктури (Національна транспортна стратегія України на період
            до 2030 року, схвалена розпорядженням Кабінету Міністрів України від{' '}
            <time dateTime="2018-05-30">30.05.2018</time> № 430-р).
          </li>
        </ul>
      </section>
      <section className="container">
        <h2>11.2 Економічні</h2>
        <p>Потенційні ризики:</p>
        <ul>
          <li>
            Дефіцит коштів у бюджеті Львівської{' '}
            <abbr title="Міська територіальна громада">МТГ</abbr> проєкти, які
            скеровані на розвиток велосипедної інфраструктури;
          </li>
          <li>
            зростання економічного добробуту жителів громади сприятиме зростанню
            автомобілеволодіння та автомобілекористування. Цей фактор може мати
            як позитивний, так і негативний ефект на впровадження Програми.
            Сценарій 1 з позитивним впливом: кількість автовласників зростає,
            проте автомобілекористування у місті регулюється. Сценарій 2 з
            негативним впливом: постійне збільшення кількості приватного
            транспорту сприятиме збільшенню автомобілекористування у місті.
          </li>
        </ul>
        <p>Потенційні позитивні фактори:</p>
        <ul>
          <li>
            вартість створення велосипедної інфраструктури є нижчою за створення
            інфраструктури для інших видів транспорту;
          </li>
          <li>
            міжнародні фінансові організації збільшують матеріальну та технічну
            підтримку розвитку сталої міської мобільності, оскільки стала міська
            мобільність відповідає: цілям сталого розвитку, Європейському
            зеленому курсу та іншим міжнародним політикам;
          </li>
          <li>
            зростання ціни на пальне може вплинути на зменшення
            автомобілекорситування і вибору альтернативних засобів мобільності,
            зокрема велосипедів та іншого{' '}
            <abbr title="Легкий персональний електричний транспортний засіб">
              ЛПТ
            </abbr>
            .
          </li>
        </ul>
      </section>
      <section className="container">
        <h2>11.3 Соціальні</h2>
        <p>Потенційні ризики:</p>
        <ul>
          <li>
            зростання числа автовласників, може сприяти зростанню невдоволення
            щодо розвитку велоінфраструктури.
          </li>
        </ul>
        <p>Потенційні позитивні фактори:</p>
        <ul>
          <li>
            збільшення спільноти користувачів велоінфраструктури. Розвиток
            велосипедної інфраструктури сприятиме збільшенню кількості
            користувачів, згуртовуватиме та об'єднуватиме жителів у неформальні
            та формальні спільноти;
          </li>
          <li>
            збільшення очікувань жителів щодо якості та стану велоінфраструктури
            та веломережі буде стимулювати реалізацію Програми та сприятиме
            постійному вдосконаленню велоінфраструктури.
          </li>
        </ul>
      </section>
      <section className="container">
        <h2>11.4 Технічні</h2>
        <p>Потенційні ризики:</p>
        <ul>
          <li>
            вузькі вулиці та обмежений вуличний простір у центральній частині
            міста не дозволятимуть виділяти простір для велосипедистів на
            магістральних маршрутах;
          </li>
          <li>
            наявність значного перепаду висот на ділянках маршрутів на території
            міста та Львівської <abbr>МТГ</abbr>;
          </li>
          <li>
            низька обізнаність у нормах та стандартах усіх учасників будівництва
            велосипедної інфраструктури може негативно вплинути на її якість;
          </li>
          <li>
            відсутність техніки для механізованого облаштування велосипедної
            інфраструктури негативно впливатиме на її якість, особливо рівність
            покриття, що через некомфортність їзди стимулюватиме користувачів
            відмовлятись від їзди на велосипеді на користь більш комфортних
            транспортних засобів.
          </li>
        </ul>
      </section>
      <section className="container">
        <h2>11.5 Правові</h2>
        <p>Потенційні ризики:</p>
        <ul>
          <li>
            збільшення інтенсивності штрафування велосипедистів за порушення{' '}
            <abbr title="Правила дорожнього руху">ПДР</abbr> через їхню
            недосконалість та недостатній розвиток велоінфраструктури, що може
            зменшити велокористування у межах Львівської <abbr>МТГ</abbr>;
          </li>
          <li>
            неготовність Патрульної поліції України приймати певні
            інфраструктурні рішення через відсутність практики їх застосування у
            м. Львові та громаді або відсутності таких рішень у чинних державних
            нормах та стандартах.
          </li>
        </ul>
      </section>
      <section className="container">
        <h2>11.6 Екологічні</h2>
        <p>Потенційні ризики:</p>
        <ul>
          <li>
            зростання забруднення атмосферного повітря може відштовхнути людей
            від користування велосипедом;
          </li>
          <li>
            зміни клімату чи інші природні катаклізми (надмірна спека, зростання
            кількості опадів) можуть негативно вплинути на реалізацію Програми.
          </li>
        </ul>
      </section>
      <div className="container">
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead title={title} themeColor="#cc3333" />
);

export default Page;
