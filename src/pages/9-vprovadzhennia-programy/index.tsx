import React, { FC } from 'react';
import { HeadFC, Link } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { ChapterHeader } from '../../components/ChapterHeader';
import { Pagination } from '../../components/Pagination';
import { PriorityIndicator } from '../../components/PriorityIndicator';
import { meta } from './_meta';
import { meta as prevMeta } from '../8-4-inshi-zavdannia/_meta';
import { meta as nextMeta } from '../10-finansove-zabezpechennia-vykonannia-programy/_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout alternativeHeader>
      <ChapterHeader title={title} order={meta.order} />
      <div className="container">
        <p>
          Впровадження Програми відбуватиметься у 2023–2033 роках відповідно до
          Перспективної велосипедної мережі Львівської{' '}
          <abbr title="Міська територіальна громада">МТГ</abbr> (
          <Link to="/dodatky/1-perspektyvna-velosypedna-merezha-lvivskoi-mth-typy-marshrutiv">
            додаток 1
          </Link>{' '}
          до цієї Програми) та Плану дій з будівництва та облаштування
          велосипедної мережі Львівської <abbr>МТГ</abbr> (
          <Link to="/dodatky/2-plan-dii-z-budivnytstva-ta-oblashtuvannia-velosypednoi-merezhi-lvivskoi-mth">
            додаток 2
          </Link>{' '}
          до цієї Програми).
        </p>
        <p>
          З метою отримання найбільшого ефекту від реалізації Програми на основі
          Перспективної велосипедної мережі Львівської <abbr>МТГ</abbr>{' '}
          розроблено План дій з будівництва та облаштування велосипедної мережі
          Львівської <abbr>МТГ</abbr>, що визначає розвиток велосипедної мережі
          за конкретними ділянками вулично-дорожньої мережі, відповідно до таких
          пріоритетів (додатки{' '}
          <Link to="/dodatky/2-plan-dii-z-budivnytstva-ta-oblashtuvannia-velosypednoi-merezhi-lvivskoi-mth">
            2
          </Link>
          ,{' '}
          <Link to="/dodatky/3-perspektyvna-velosypedna-merezha-lvivskoi-mth-priorytetnist">
            3
          </Link>{' '}
          до цієї Програми):
        </p>
        <ol>
          <li>
            <PriorityIndicator value={1} /> І пріоритет: З’єднання наявної
            велосипедної мережі, загальна протяжність – 19,15 км.
          </li>
          <li>
            <PriorityIndicator value={2} /> ІІ пріоритет: Будівництво та
            облаштування нових ключових ділянок велосипедної мережі, загальна
            протяжність – 109,84 км.
          </li>
          <li>
            <PriorityIndicator value={3} /> ІІІ пріоритет: Будівництво нової
            велосипедної мережі, загальна протяжність – 116,94 км.
          </li>
          <li>
            <PriorityIndicator value={4} /> ІV пріоритет: Будівництво інших
            ділянок велосипедної мережі, загальна протяжність – 201,62 км.
          </li>
        </ol>
        <p>
          Кожен пріоритет складається з перспективних ділянок велосипедної
          мережі; виду робіт, які необхідно провести; їх протяжності та
          замовників робіт.
        </p>
        <p>
          Інформація, наведена у додатку 2 до цієї Програми, є орієнтовною і
          допускає обґрунтовані зміни у процесі планування, проєктування та
          реалізації.
        </p>
        <p>Загальна протяжність наявної велосипедної мережі – 136,48 км.</p>
        <p>Загальна протяжність планованої велосипедної мережі – 447,55 км.</p>
        <p>
          Загальна орієнтовна протяжність наявної та планованої велосипедної
          мережі – 584 км.
        </p>
        <p>
          Під час розробки велосипедної мережі сформовано мережу більш
          розгалужену, ніж буде реально реалізувати за наступні 10 років. Це
          зроблено для того, щоб з одного боку можна було показати ширшу
          перспективу розвитку велосипедної мережі, а з іншого, у разі
          проведення ремонтних робіт на певній ділянці вулично-дорожньої мережі,
          можна було відразу врахувати потреби розвитку велосипедної
          інфраструктури, оскільки складно передбачити, які саме ділянки
          зазнають змін у 10-річній перспективі.
        </p>
        <p>
          Також належність певної ділянки велосипедної мережі до визначеного
          пріоритету не обовʼязково означає послідовність її реалізації, а є
          інструментом для формування титульних списків та планів робіт на рік.
        </p>
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead title={title} themeColor="#cc3333" />
);
export default Page;
