import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { MobilityChart } from '../../components/MobilityChart';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { ChapterHeader } from '../../components/ChapterHeader';
import { SubNavigation } from '../../components/SubNavigation';
import { Pagination } from '../../components/Pagination';
import { meta } from './_meta';
import { meta as child1 } from '../2-1-osnovni-rezultaty-doslidzhennia/_meta';
import { meta as child2 } from '../2-2-otsinka-umov-peresuvannia-na-velosypedi/_meta';
import { meta as prevMeta } from '../1-2-zviazok-programy-z-inshymy-stratehichnymy-dokumentamy-mista/_meta';
import * as classes from './Page.module.scss';

const title = meta.title;
const navItems = [
  {
    label: child1.title,
    href: `/${child1.slug}`,
  },
  {
    label: child2.title,
    href: `/${child2.slug}`,
  },
];

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout alternativeHeader>
      <ChapterHeader title={title} order={meta.order} />
      <div className="container">
        <p>
          У Львові особлива увага приділяється аналітичним даним з метою
          розуміння потреб і побажань жителів незалежно від їхнього способу
          переміщення містом. Такий підхід дозволяє ставати місту більш дружнім
          до різних цільових аудиторій. Відповідно в межах дослідження та
          визначення частки переміщень за переважаючими видами мобільності серед
          жителів Львівської{' '}
          <abbr title="Міська територіальна громада">МТГ</abbr> у 2019 та 2021
          роках було визначено, що в теплий період року для 6% жителів велосипед
          є основним видом мобільності, а вже у 2021 році цей відсоток
          збільшився на 2% і становить 8%. Таке ж дослідження було проведено й в
          холодний період 2021 року. Воно показало, що для 4 % жителів
          Львівської <abbr>МТГ</abbr> велосипед є основним видом мобільності.
        </p>
        <p>
          Загальна картина переміщень за видами мобільності у Львові та
          Львівській <abbr>МТГ</abbr> виглядає так:
        </p>
      </div>

      <MobilityChart />

      <div className="container">
        <SubNavigation items={navItems} className={classes.children} />
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: child1.title,
            pathname: `/${child1.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead title={title} themeColor="#cc3333"  />
);

export default Page;
