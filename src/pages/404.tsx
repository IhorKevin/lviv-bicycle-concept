import React from 'react';
import { Link, HeadFC, PageProps } from 'gatsby';

const pageStyles = {
  color: '#232129',
  padding: '96px',
  fontFamily: 'var(--serif-font-family)',
};
const headingStyles = {
  marginTop: 0,
  marginBottom: 64,
  maxWidth: 320,
};

const paragraphStyles = {
  marginBottom: 48,
};

const NotFoundPage: React.FC<PageProps> = () => {
  return (
    <main style={pageStyles}>
      <h1 style={headingStyles}>Сторінку не знайдено</h1>
      <p style={paragraphStyles}>
        За цим посиланням сторінка не існує, або ви не маєте доступу до вмісту.
        <br />
        <Link to="/">На головну</Link>.
      </p>
    </main>
  );
};

export default NotFoundPage;

export const Head: HeadFC = () => <title>Не знайдено</title>;
