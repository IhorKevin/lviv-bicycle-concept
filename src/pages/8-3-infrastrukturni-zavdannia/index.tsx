import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { TaskAssignee } from '../../components/TaskAssignee';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { Pagination } from '../../components/Pagination';
import { meta } from './_meta';
import { meta as prevMeta } from '../8-2-instytutsiini-zavdannia/_meta';
import { meta as nextMeta } from '../8-4-inshi-zavdannia/_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout>
      <div className="container">
        <h1>
          {meta.order} {title}
        </h1>
        <p>
          8.3.1 Забезпечувати повноцінне відновлення велосипедної інфраструктури
          у разі порушень благоустрою до попереднього або кращого стану.
        </p>
        <TaskAssignee
          name="балансоутримувачі вулично-дорожньої мережі"
          deadline="постійно"
        />
        <p>
          8.3.2 На ділянках наявної велосипедної інфраструктури проаналізувати
          наявність та стан супутньої пішохідної інфраструктури. Передати
          напрацювання балансоутримувачам для їх усунення.
        </p>
        <TaskAssignee
          name="департамент міської мобільності та вуличної інфраструктури"
          deadline="квітень 2024 року"
        />
        <p>
          8.3.3 Слідкувати за станом дорожніх знаків та розмітки, здійснювати їх
          заміну та оновлення, відповідно до чинних{' '}
          <abbr title="Державні стандарти України">ДСТУ</abbr> та{' '}
          <abbr title="Правила дорожнього руху">ПДР</abbr>.
        </p>
        <TaskAssignee name="балансоутримувачі територій" deadline="постійно" />
        <p>
          8.3.4 Облаштовувати велосипедні парковки (велостійки) поруч з
          комунальними закладами освіти, охорони здоров’я, культури, центрів
          надання послуг відповідно до <abbr>ДСТУ</abbr>.
        </p>
        <TaskAssignee
          name="департамент гуманітарної політики, департамент розвитку, департамент адміністративних послуг, районні адміністрації, замовники проведення робіт"
          multi
          deadline="постійно"
        />
        <p>
          8.3.5 Облаштовувати велосипедні парковки на транспортно-пересадкових
          вузлах, які будуть визначені у межах проведення оптимізації маршрутної
          мережі громадського транспорту.
        </p>
        <TaskAssignee
          name="районні адміністрації"
          multi
          deadline="з 2024 року щорічно"
        />
        <p>
          8.3.6 Позначати перспективні альтернативні та рекреаційні велосипедні
          маршрути на місцевості згідно з розробленим дизайн-кодом.
        </p>
        <TaskAssignee
          name="балансоутримувачі вулично-дорожньої мережі"
          multi
          deadline="у межах реалізації маршрутів"
        />
        <p>8.3.7 Забезпечити навігацію магістральних велосипедних маршрутів.</p>
        <TaskAssignee
          name="балансоутримувачі вулично-дорожньої мережі"
          multi
          deadline="з 2025 року щорічно"
        />
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <PageHead title={title} />;

export default Page;
