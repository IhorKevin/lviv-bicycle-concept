import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { ChapterHeader } from '../../components/ChapterHeader';
import { SubNavigation } from '../../components/SubNavigation';
import { Pagination } from '../../components/Pagination';
import { meta } from './_meta';
import { meta as child1 } from '../8-1-stratehichni-ta-rehuliatorni-zavdannia/_meta';
import { meta as child2 } from '../8-2-instytutsiini-zavdannia/_meta';
import { meta as child3 } from '../8-3-infrastrukturni-zavdannia/_meta';
import { meta as child4 } from '../8-4-inshi-zavdannia/_meta';
import { meta as prevMeta } from '../7-3-tymchasovi-zakhody-z-rozvytku-velomerezhi/_meta';

const title = meta.title;
const navItems = [
  {
    label: child1.title,
    href: `/${child1.slug}`,
  },
  {
    label: child2.title,
    href: `/${child2.slug}`,
  },
  {
    label: child3.title,
    href: `/${child3.slug}`,
  },
  {
    label: child4.title,
    href: `/${child4.slug}`,
  },
];

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout alternativeHeader>
      <ChapterHeader title={title} order={meta.order} />
      <div className="container">
        <SubNavigation items={navItems} />
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: child1.title,
            pathname: `/${child1.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <PageHead title={title} themeColor="#cc3333" />;

export default Page;
