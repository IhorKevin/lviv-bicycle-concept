import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { meta } from './_meta';
import * as classes from './Page.module.scss';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  const tableCN = ['table', classes.table].join(' ');
  return (
    <Layout>
      <div className="container section">
        <h1>{title}</h1>
        <div className="table-wrapper">
          <table className={tableCN}>
            <tbody>
              <tr>
                <td>1</td>
                <th>Розробники Програми</th>
                <td>
                  Департамент міської мобільності та вуличної інфраструктури,
                  комунальна установа Інститут міста, робоча група з розвитку
                  велосипедної мережі та інфраструктури
                </td>
              </tr>
              <tr>
                <td>2</td>
                <th>Співрозробники Програми</th>
                <td>
                  Управління архітектури та урбаністики департаменту
                  містобудування,{' '}
                  <abbr title="Львівське комунальне підприємство">ЛКП</abbr>{' '}
                  "Львівавтодор",{' '}
                  <a href="https://lav.org.ua">
                    <abbr title="Громадська організація">ГО</abbr> "Львівська
                    асоціація велосипедистів"
                  </a>
                  , Німецьке товариство міжнародного співробітництва (GIZ) GmbH
                </td>
              </tr>
              <tr>
                <td>3</td>
                <th>Виконавці</th>
                <td>
                  Департамент міської мобільності та вуличної інфраструктури,{' '}
                  <abbr>ЛКП</abbr> "Львівавтодор", районні адміністрації
                </td>
              </tr>
              <tr>
                <td>4</td>
                <th>Користувачі та інші зацікавлені сторони</th>
                <td>
                  Мешканці Львівської міської територіальної громади, гості
                  міста, міські служби, які забезпечують життєдіяльність міста,
                  компанії з логістики та доставки, малий та середній бізнес
                </td>
              </tr>
              <tr>
                <td>5</td>
                <th>Період дії</th>
                <td>2023–2033 роки</td>
              </tr>
              <tr>
                <td>6</td>
                <th>Зв’язок з іншими стратегічними документами</th>
                <td>
                  <ul>
                    <li>
                      План сталої міської мобільності м. Львова, затверджений
                      ухвалою міської ради від{' '}
                      <time dateTime="2020-02-13">13.02.2020</time>{' '}
                      <a
                        href="https://www8.city-adm.lviv.ua/inteam/uhvaly.nsf/(SearchForWeb)/3C1CE1DA7327E2CFC2258512002F9A87?OpenDocument"
                        target="_blank"
                        rel="noreferrer"
                      >
                        №6293
                      </a>
                    </li>
                    <li>
                      Інтегрована концепція розвитку: Львів 2030, затверджена
                      ухвалою міської ради від{' '}
                      <time dateTime="2021-07-09">09.07.2021</time> №1131{' '}
                      <a
                        href="https://www8.city-adm.lviv.ua/inTEAM/Uhvaly.NSF/(SearchForWeb)/9C79AF352E47829AC2258718001FC021?OpenDocument"
                        target="_blank"
                        rel="noreferrer"
                      >
                        №1131
                      </a>
                    </li>
                    <li>
                      План заходів "Зеленого міста" для м. Львова" до 2035 року,
                      затверджений ухвалою міської ради від{' '}
                      <time dateTime="2021-07-09">09.07.2021</time>{' '}
                      <a
                        href="https://www8.city-adm.lviv.ua/inteam/uhvaly.nsf/(SearchForWeb)/6AF12CC43C6A8042C225871B004330C3?OpenDocument"
                        target="_blank"
                        rel="noreferrer"
                      >
                        №1238
                      </a>
                    </li>
                    <li>
                      Стратегія прориву 2027, затверджена ухвалою міської ради
                      від <time dateTime="2019-06-20">20.06.2019</time>{' '}
                      <a
                        href="https://www8.city-adm.lviv.ua/inTEAM/Uhvaly.NSF/(SearchForWeb)/8B77715199AC9DC5C225842A0049BC0E?OpenDocument"
                        target="_blank"
                        rel="noreferrer"
                      >
                        №5176
                      </a>
                    </li>
                  </ul>
                </td>
              </tr>
              <tr>
                <td>7</td>
                <th>Джерела фінансування</th>
                <td>
                  Бюджет Львівської міської територіальної громади, Державний
                  бюджет України та інші кошти, не заборонені законодавством
                  України
                </td>
              </tr>
              <tr>
                <td>8</td>
                <th>
                  Загальний обсяг фінансових ресурсів, необхідних для реалізації
                  Програми
                </th>
                <td>
                  У межах видатків, які передбачені у бюджеті Львівської міської
                  територіальної громади на відповідний бюджетний період
                </td>
              </tr>
              <tr>
                <td>9</td>
                <th>Очікувані результати</th>
                <td>
                  <ul>
                    <li>
                      зменшення кількості{' '}
                      <abbr title="Дорожня транспортна пригода">ДТП</abbr> за
                      участі велосипедистів та інших учасників дорожнього руху;
                    </li>
                    <li>
                      досягнення рівня нульової смертності велосипедистів
                      внаслідок <abbr>ДТП</abbr>;
                    </li>
                    <li>
                      з’єднання між собою всіх ділянок велосипедної мережі у
                      межах території Львівської міської територіальної громади
                      (для досягнення регулярного користування мешканцями
                      велосипедом, як мінімум – 15%)
                    </li>
                  </ul>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <PageHead title={title} />;

export default Page;
