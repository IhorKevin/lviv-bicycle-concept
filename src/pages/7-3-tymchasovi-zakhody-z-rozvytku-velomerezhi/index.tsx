import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { Pagination } from '../../components/Pagination';
import { meta } from './_meta';
import { meta as prevMeta } from '../7-2-velosypedna-infrastruktura/_meta';
import { meta as nextMeta } from '../8-zavdannia-programy/_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout>
      <div className="container">
        <h1>
          {meta.order} {title}
        </h1>
        <p>
          З метою більш швидкого розвитку велосипедної мережі та перевірки
          проєктних рішень допускається облаштування тимчасової велосипедної
          інфраструктури за допомогою інструментів тактичного урбанізму,
          відповідно до рішення виконавчого комітету від{' '}
          <time dateTime="2020-07-24">24.07.2020</time> № 634 "Про затвердження
          Положення про процедуру запровадження тимчасових змін до вуличного
          простору м. Львова" із подальшою перспективою капітального виконання.
        </p>
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <PageHead title={title} />;

export default Page;
