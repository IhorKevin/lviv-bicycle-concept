import React, { FC } from 'react';
import { HeadFC, Link } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { Pagination } from '../../components/Pagination';
import { ChapterHeader } from '../../components/ChapterHeader';
import { meta } from './_meta';
import { meta as prevMeta } from '../11-faktory-ta-ryzyky-shcho-mozhut-vplyvaty-na-realizatsiiu-programy/_meta';
import { meta as nextMeta } from '../13-ochikuvani-rezultaty-vid-realizatsii-zavdan-programy-u-2033-rotsi/_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout alternativeHeader>
      <ChapterHeader title={title} order={meta.order} />
      <div className="container">
        <p>
          Основними координатором та виконавцем Програми є департамент міської
          мобільності та вуличної інфраструктури. Також виконавцями Програми є
          департамент житлового господарства та інфраструктури, Галицька районна
          адміністрація, Залізнична районна адміністрація, Франківська районна
          адміністрація, Личаківська районна адміністрація, Сихівська районна
          адміністрація, Шевченківська районна адміністрація,{' '}
          <abbr title="Львівське комунальне підприємство">ЛКП</abbr>{' '}
          "Львівавтодор".
        </p>
        <p>
          Моніторинг реалізації Програми здійснює департамент міської
          мобільності та вуличної інфраструктури за підсумками року та звітує
          заступнику міського голови з питань житлово-комунального господарства.
        </p>
        <p>
          Моніторинг виконання Програми, здійснюватиметься за такими
          індикаторами:
        </p>
        <ul>
          <li>
            кількість <abbr title="Дорожньо-транспортна пригода">ДТП</abbr> за
            участі велосипедистів з постраждалими/загиблими;
          </li>
          <li>
            протяжність реалізованої велоінфраструктури згідно з{' '}
            <Link
              to={
                '/dodatky/2-plan-dii-z-budivnytstva-ta-oblashtuvannia-velosypednoi-merezhi-lvivskoi-mth'
              }
            >
              додатком 2
            </Link>{' '}
            до цієї Програми;
          </li>
          <li>загальна протяжність неперервної велосипедної мережі;</li>
          <li>
            частка переміщень на велосипеді та іншому{' '}
            <abbr title="Легкий персональний електричний транспортний засіб">
              ЛПТ
            </abbr>{' '}
            в загальній частці переміщень жителів Львівської{' '}
            <abbr title="Міська територіальна громада">МТГ</abbr> (
            <span lang="en">modal split</span>) (моніторинг проводиться не рідше
            ніж раз на чотири роки);
          </li>
          <li>
            кількість населених пунктів Львівської <abbr>МТГ</abbr>, які
            з'єднані велосипедною мережею зі Львовом;
          </li>
          <li>
            рівень задоволеності населення станом велосипедної інфраструктури
            (залежить від можливості проведення моніторингу якості життя
            громади);
          </li>
          <li>
            частка переміщень на велосипеді та іншому <abbr>ЛПТ</abbr> в
            холодний період року (залежить від можливості проведення дослідження
            мобільності мешканців громади);
          </li>
          <li>
            частка переміщень на велосипеді та іншому <abbr>ЛПТ</abbr> в теплий
            період року (залежить від можливості проведення дослідження
            мобільності мешканців громади);
          </li>
          <li>частка велокористування серед жінок;</li>
          <li>
            кількість мобільних хабів (транспортно-пересадкових вузлів), які
            обладнані місцями для безпечного тимчасового зберігання велосипеда;
          </li>
          <li>
            кількість комунальних закладів середньої освіти, культури, охорони
            здоров’я та адміністративних будівель, які обладнані місцями для
            безпечного тимчасового зберігання велосипеда;
          </li>
          <li>
            кількість встановлених велостійок у Львівській <abbr>МТГ</abbr> (за
            період проведення моніторингу).
          </li>
        </ul>
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead title={title} themeColor="#cc3333" />
);

export default Page;
