import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { ChapterHeader } from '../../components/ChapterHeader';
import { Pagination } from '../../components/Pagination';
import { meta } from './_meta';
import { meta as prevMeta } from '../4-viziia/_meta';
import { meta as nextMeta } from '../6-pryntsypy-rozvytku-velomerezhi-ta-veloinfrastruktury/_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout alternativeHeader>
      <ChapterHeader title={title} order={meta.order} />
      <div className="container">
        <h2>Ціль 1</h2>
        <p>
          Підняти частку регулярного користування велосипедом та іншим{' '}
          <abbr title="Легкий персональний електричний транспортний засіб">
            ЛПТ
          </abbr>{' '}
          як транспортом до як мінімум 15% для жителів Львівської МТГ.
        </p>
        <h2>Ціль 2</h2>
        <p>
          Облаштувати не менше 150 км додаткової велосипедної інфраструктури до
          2033 року.
        </p>
        <h2>Ціль 3</h2>
        <p>
          З’єднати оптимальними велосипедними маршрутами усі частини Львівської{' '}
          <abbr title="Міська територіальна громада">МТГ</abbr> з центром міста
          та між собою.
        </p>
        <h2>Ціль 4</h2>
        <p> Забезпечити цілісну велосипедну мережу в межах м. Львова.</p>
        <h2>Ціль 5</h2>
        <p>Інтегрувати велосипедний транспорт із іншими видами мобільності.</p>
        <h2>Ціль 6</h2>
        <p>
          Забезпечити можливість надійного зберігання велосипеда поруч з домом
          та паркування біля місця роботи/навчання для жителів Львівської{' '}
          <abbr>МТГ</abbr>.
        </p>
        <h2>Ціль 7</h2>
        <p>
          Підвищити культуру використання велосипеда як транспортного засобу.
        </p>
        <h2>Ціль 8</h2>
        <p>
          Посилити інституційну спроможність Львівської міської ради у розвитку
          велосипедної інфраструктури.
        </p>
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead title={title} themeColor="#cc3333"  />
);

export default Page;
