export const meta: PageMeta = {
  slug: '6-pryntsypy-rozvytku-velomerezhi-ta-veloinfrastruktury',
  title: 'Принципи розвитку веломережі та велоінфраструктури',
  shortTitle: 'Принципи',
  order: '6',
};
