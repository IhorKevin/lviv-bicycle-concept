import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { TaskAssignee } from '../../components/TaskAssignee';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { Pagination } from '../../components/Pagination';
import { meta } from './_meta';
import { meta as prevMeta } from '../8-3-infrastrukturni-zavdannia/_meta';
import { meta as nextMeta } from '../9-vprovadzhennia-programy/_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout>
      <div className="container">
        <h1>
          {meta.order} {title}
        </h1>
        <p>
          8.4.1 Проводити очищення велоінфраструктури від снігу не пізніше ніж
          після 12 годин з часу початку снігопаду.
        </p>
        <TaskAssignee
          name="балансоутримувачі вулично-дорожньої мережі"
          multi
          deadline="постійно"
        />
        <p>
          8.4.2 Популяризувати безпечну їзду на велосипеді та використання
          велосипеда як транспортного засобу за допомогою соціальної реклами та
          різноманітних акцій серед жителів Львівської <abbr>МТГ</abbr>.
        </p>
        <TaskAssignee
          name={
            'департамент міської мобільності та вуличної інфраструктури, управління комунікації департаменту "Адміністрація міського голови"'
          }
          multi
          deadline="щорічно"
        />
        <p>
          8.4.3 Працювати над включенням Львова та Львівської <abbr>МТГ</abbr> у
          рейтинги, що відзначають досягнення міст у галузі розвитку
          велосипедної мережі та велосипедної інфраструктури.
        </p>
        <TaskAssignee
          name="комунальна установа Інститут міста, департамент міської мобільності та вуличної інфраструктури"
          multi
          deadline="постійно"
        />
        <p>
          8.4.4 Здійснювати пошук грантів для встановлення велосипедних
          лічильників на головних магістральних велосипедних маршрутах
          Львівської <abbr>МТГ</abbr>.
        </p>
        <TaskAssignee
          name="департамент міської мобільності та вуличної інфраструктури, комунальна установа Інститут міста"
          multi
          deadline="постійно"
        />
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <PageHead title={title} />;

export default Page;
