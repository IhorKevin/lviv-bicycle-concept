import React, { FC } from 'react';
import { HeadFC } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { Pagination } from '../../components/Pagination';
import { siteUrl } from '../../../gatsby-config';
import { meta } from './_meta';
import { meta as prevMeta } from '../2-2-otsinka-umov-peresuvannia-na-velosypedi/_meta';
import { meta as nextMeta } from '../4-viziia/_meta';
import cyclistSquare from './велосипедист_1-1.webp';
import cyclistPortraitSmall from './велосипедист_3-4_640.webp';
import cyclistPortraitLarge from './велосипедист_3-4_1280.webp';
import ogImage from './og-image.jpg';
import * as classes from './Page.module.scss';

const title = meta.title;
const photoAltText =
  'Чоловік у шоломі, спираючись на велосипед, тримає плакат з написом "Ми — велосипедисти/ки з купорічним стажем вимагаємо велодоріжок!"';

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout>
      <div className={classes.section}>
        <div className={classes.grid}>
          <div className={classes.content}>
            <h1 className={classes.title}>
              {meta.order} {title}
            </h1>
            <p className={classes.lead}>
              Забезпечити <em>комфортне</em> та <em>безпечне</em> користування{' '}
              <em>велосипедом</em> на всій вулично-дорожній мережі Львівської{' '}
              <abbr title="Міська територіальна громада">МТГ</abbr>, створивши
              умови для використання велосипеда та іншого{' '}
              <abbr title="Легкий персональний електричний транспортний засіб">
                ЛПТ
              </abbr>{' '}
              як повноцінного транспортного засобу.
            </p>
          </div>
          <div className={classes.illustration}>
            <picture>
              <source
                srcSet={`${cyclistPortraitSmall} 1x, ${cyclistPortraitLarge} 2x`}
                type="image/webp"
                media="(min-width: 1024px)"
              />
              <source srcSet={cyclistSquare} type="image/webp" sizes="100vw" />
              <img
                src={ogImage}
                alt={photoAltText}
                className={classes.image}
                width="100%"
                height="412"
              />
            </picture>
          </div>
        </div>
      </div>
      <div className="container">
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead title={title}  og={{ image: `${siteUrl}${ogImage}` }} />
);

export default Page;
