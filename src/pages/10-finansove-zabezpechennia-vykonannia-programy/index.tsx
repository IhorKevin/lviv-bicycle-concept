import React, { FC } from 'react';
import { HeadFC, Link } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { Pagination } from '../../components/Pagination';
import { ChapterHeader } from '../../components/ChapterHeader';
import { meta } from './_meta';
import { meta as prevMeta } from '../9-vprovadzhennia-programy/_meta';
import { meta as nextMeta } from '../11-faktory-ta-ryzyky-shcho-mozhut-vplyvaty-na-realizatsiiu-programy/_meta';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  return (
    <Layout alternativeHeader>
      <ChapterHeader title={title} order={meta.order} />
      <div className="container">
        <p>
          Фінансування Програми здійснюється за рахунок видатків бюджету
          розвитку бюджету Львівської міської територіальної громади та інших
          джерел фінансування, зокрема коштів з Державного бюджету України та
          інших коштів, не заборонених законодавством України.
        </p>
        <p>
          У частині реалізації Програми департамент міської мобільності та
          вуличної інфраструктури щороку надає пропозиції щодо будівництва,
          реконструкції, ремонту міської інфраструктури на створення нових та
          зміну наявних велосипедних і пішохідних зв’язків відповідно до
          додатків{' '}
          <Link to="/dodatky/1-perspektyvna-velosypedna-merezha-lvivskoi-mth-typy-marshrutiv">
            1
          </Link>
          ,{' '}
          <Link to="/dodatky/2-plan-dii-z-budivnytstva-ta-oblashtuvannia-velosypednoi-merezhi-lvivskoi-mth">
            2
          </Link>{' '}
          до цієї Програми.
        </p>
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead title={title} themeColor="#cc3333" />
);

export default Page;
