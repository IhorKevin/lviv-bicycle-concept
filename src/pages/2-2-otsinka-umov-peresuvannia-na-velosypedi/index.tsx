import React, { FC, useEffect, useState } from 'react';
import { HeadFC } from 'gatsby';
import { MobilityTypeCard } from '../../components/MobilityTypeCard';
import { data } from './_data';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { Pagination } from '../../components/Pagination';
import { meta } from './_meta';
import { meta as prevMeta } from '../2-1-osnovni-rezultaty-doslidzhennia/_meta';
import { meta as nextMeta } from '../3-meta-programy/_meta';
import * as classes from './Page.module.scss';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  const [sorting, setSorting] = useState<string>('');
  const [sortedItems, setSortedItems] = useState<typeof data>(data);

  useEffect(() => {
    if (sorting === 'safety' || sorting === 'comfort') {
      const newItems = [...data].sort((a, b) => {
        return a[sorting] > b[sorting] ? -1 : 1;
      });
      setSortedItems(newItems);
    } else {
      setSortedItems(data);
    }
  }, [sorting]);

  const getButtonCN = (key: string): string => {
    const classNames = [classes.button];
    if (key === sorting) classNames.push(classes.active);
    return classNames.join(' ');
  };

  return (
    <Layout>
      <div className="container">
        <h1>
          {meta.order} {title}
        </h1>
        <p>
          Учасникам і учасницям{' '}
          <abbr title="Фокус-групові дослідження">ФГД</abbr> було запропоновано
          24 можливих умов пересування на велосипеді у місті для оцінки їх крізь
          призму 1) безпеки (зручності пересування) та 2) комфорту (якості умов
          пересування). Для вимірювання застосовувалася 5-бальна шкала, де 1 –
          цілком незручно/некомфортно, а 5 – цілком зручно/комфортно. У таблиці
          представлено середні значення (варіант відповіді "Важко відповісти"
          віднесено до пропущених значень).
        </p>
        <p>
          Відповідно до результатів опитування загалом користувачам і
          користувачкам велосипеду запропоновані умови є однаково і безпечними
          (3,44), і комфортними (3,50 із 5 можливих). Оцінка різниться радше у
          розрізі конкретно вимірюваного показника.
        </p>
        <p>
          За параметрами комфорту та безпеки є 2 атрибути, які посідають
          лідерську сходинку:
        </p>
        <ol>
          <li>
            Виокремлена велодоріжка від автотранспорту та пішохідного руху за
            допомогою озеленення (4,95 – оцінка умов безпеки та 5,00 – оцінка
            умов комфорту).
          </li>
          <li>
            Виокремлена двостороння велодоріжка від автотранспорту, пішохідного
            руху та від зустрічного велоруху (4,95 – оцінка умов безпеки та 4,85
            – оцінка умов комфорту).
          </li>
        </ol>
        <p>
          Найнижчу оцінку за 2 критеріями посів тип вулиці, яка замощена
          бруківкою із прокладеними трамвайними коліями (1,54 – усереднений
          показник, де 1,73 – оцінка умов безпеки та 1,35 – оцінка умов
          комфорту).
        </p>

        <div className={classes.sorting}>
          <button
            type="button"
            className={getButtonCN('safety')}
            onClick={(event) => {
              setSorting(sorting === 'safety' ? '' : 'safety');
            }}
          >
            Безпечні
          </button>
          <button
            type="button"
            className={getButtonCN('comfort')}
            onClick={(event) => {
              setSorting(sorting === 'comfort' ? '' : 'comfort');
            }}
          >
            Комфортні
          </button>
        </div>

        {sortedItems.map((item, index) => (
          <MobilityTypeCard
            key={item.title + item.comfort}
            imageSlug={item.imageSlug}
            title={item.title}
            summary={item.summary}
            safety={item.safety}
            comfort={item.comfort}
          />
        ))}
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => <PageHead title={title} />;

export default Page;
