type Item = {
  imageSlug: string;
  title: string;
  summary: string;
  safety: number;
  comfort: number;
};

export const data: Item[] = [
  {
    imageSlug: 'vulytsia-vnutrishniokvartalna',
    title: 'Внутрішньоквартальна вулиця',
    summary: 'З обмеженням швидкості 30 км/год (асфальт)',
    safety: 4.1,
    comfort: 4.35,
  },
  {
    imageSlug: 'velosmuha-vyokremlena-bordur',
    title: 'Виокремлена велосмуга',
    summary: 'За допомогою бордюра, делінеатора, горщика з рослиною тощо',
    safety: 4.46,
    comfort: 4.71,
  },
  {
    imageSlug: 'vulytsia-hruba-brukivka-kolii',
    title: 'Вулиця, замощена грубою бруківкою',
    summary: 'З прокладеними трамвайними коліями',
    safety: 1.73,
    comfort: 1.35,
  },
  {
    imageSlug: 'velodorizhka-dvostoronnya-vyokremlena',
    title: 'Виокремлена двостороння велодоріжка',
    summary: 'Від автотранспорту та від зустрічного велоруху',
    safety: 4.95,
    comfort: 4.85,
  },
  {
    imageSlug: 'velosmuha-nevyokremlena',
    title: 'Невиокремлена велосмуга',
    summary: 'З гладким покриттям',
    safety: 2.89,
    comfort: 3.26,
  },
  {
    imageSlug: 'velodorizhka-vyokremlena',
    title: 'Виокремлена велодоріжка',
    summary: 'Від автотранспорту та пішохідного руху за допомогою озеленення',
    safety: 4.95,
    comfort: 5.0,
  },
  {
    imageSlug: 'vulytsia-brukivka-plyty',
    title: 'Вулиця, замощена грубою бруківкою',
    summary: 'З прокладеними трамвайними коліями на плитах БКВ',
    safety: 2.0,
    comfort: 2.0,
  },
  {
    imageSlug: 'velodorizhka-dvostoronnya-bez-vydilennia',
    title: 'Двостороння велодоріжка, виокремлена від проїзної частини',
    summary: 'Без виділення від зустрічного велопотоку та пішохідного руху',
    safety: 4.35,
    comfort: 4.09,
  },
  {
    imageSlug: 'pishohidna-zona-brukivka',
    title: 'Пішохідна зона',
    summary: 'Замощена грубою бруківкою',
    safety: 3.28,
    comfort: 1.71,
  },
  {
    imageSlug: 'vulytsia-asfalt',
    title: 'Вулиця з асфальтним покриттям',
    summary: 'По одній смузі руху в кожному напрямку',
    safety: 2.78,
    comfort: 3.62,
  },
  {
    imageSlug: 'velosmuha-parkuvannia',
    title: 'Велосмуга, виділена за допомогою розмітки',
    summary: 'З паркуванням праворуч',
    safety: 3.6,
    comfort: 4.12,
  },
  {
    imageSlug: 'velosmuha-bez-parkuvannia',
    title: 'Велосмуга, виділена за допомогою розмітки',
    summary: 'Асфальтне покриття, без паркування праворуч',
    safety: 4.08,
    comfort: 4.65,
  },
  {
    imageSlug: 'smuha-ht',
    title: 'Смуга для руху громадського транспорту',
    summary: 'З асфальтним покриттям',
    safety: 3.45,
    comfort: 4.12,
  },
  {
    imageSlug: 'spilna-zona',
    title: 'Спільна зона для велосипедистів та пішоходів',
    summary: '',
    safety: 3.88,
    comfort: 3.62,
  },
  {
    imageSlug: 'zhytlova-zona',
    title: 'Житлова зона',
    summary: 'З асфальтним покриттям',
    safety: 4.28,
    comfort: 4.35,
  },
  {
    imageSlug: 'pishohidna-zona-mozaika',
    title: 'Пішохідна зона',
    summary: 'Замощена дрібною бруківкою (мозаїкою)',
    safety: 3.95,
    comfort: 3.32,
  },
];
