import React, { FC, useEffect, useRef } from 'react';
import { HeadFC } from 'gatsby';
import { Layout } from '../../components/Layout';
import { PageHead } from '../../components/PageHead';
import { Pagination } from '../../components/Pagination';
import { meta } from './_meta';
import { meta as prevMeta } from '../3-meta-programy/_meta';
import { meta as nextMeta } from '../5-tsili-programy/_meta';
import coverFallback from './cover-album.jpg';
import coverPortrait from './cover-portrait.webp';
import coverAlbum from './cover-album.webp';
import girlsImage from './girls-4x5.webp';
import girlsImage621 from './girls-4x5-621.webp';
import girlsFallback from './girls-4x5.jpg';
import fatherImage from './father-4x5.webp';
import fatherImage621 from './father-4x5-621.webp';
import fatherFallback from './father-4x5.jpg';
import * as classes from './Page.module.scss';

const title = meta.title;

type Props = {};

export const Page: FC<Props> = (props) => {
  const fatherRef = useRef<HTMLImageElement | null>(null);
  const girlsRef = useRef<HTMLImageElement | null>(null);
  const lead1Ref = useRef<HTMLParagraphElement | null>(null);
  const lead2Ref = useRef<HTMLParagraphElement | null>(null);

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.classList.add(classes.visible);
          observer.unobserve(entry.target);
        }
      });
    }, {
      threshold: 0.2
    });

    if (girlsRef.current) observer.observe(girlsRef.current);
    if (fatherRef.current) observer.observe(fatherRef.current);
    if (lead1Ref.current) observer.observe(lead1Ref.current);
    if (lead2Ref.current) observer.observe(lead2Ref.current);

    return () => {
      observer.disconnect();
    };
  }, []);

  return (
    <Layout alternativeHeader>
      <div className={classes.hero}>
        <picture>
          <source
            srcSet={coverAlbum}
            type="image/webp"
            media="(min-width: 1024px)"
          />
          <source srcSet={coverPortrait} type="image/webp" />
          <img
            src={coverFallback}
            className={classes.cover}
            alt="Двоє жінок та двоє чоловіків на велосипедах рухається в одному напрямі"
          />
        </picture>
        <div className={classes.overlay}></div>
        <h1>
          {meta.order} {title}
        </h1>
      </div>
      <div className={[classes.section, classes.start].join(' ')}>
        <div>
          <div className={classes.goal}>
            15<small>%</small>
          </div>
          <p className={classes.lead} ref={lead1Ref}>
            У 2033 році велосипед у Львівській{' '}
            <abbr title="Міська територіальна громада">МТГ</abbr> – привабливий
            транспортний засіб для переважної більшості жителів, щоденно на
            велосипеді та іншому легкому персональному транспорті переміщуються
            щонайменше 15% жителів громади.
          </p>
        </div>
        <picture>
          <source
            srcSet={`${girlsImage621} 621w, ${girlsImage} 759w`}
            type="image/webp"
            sizes="calc(100vw - 4 * var(--page-padding)), (min-width:1024px) 621px"
          />
          <img
            src={girlsFallback}
            className={classes.illustration}
            width="100%"
            alt="Дві дівчини в теплому одязі їдуть велосипедами по замощеній бруківкою пішохідній зоні. Перша з них широко посміхається. Погода дощова"
            loading="lazy"
            ref={girlsRef}
          />
        </picture>
      </div>
      <div className={[classes.section, classes.end].join(' ')}>
        <picture>
          <source
            srcSet={`${fatherImage621} 621w, ${fatherImage} 1365w`}
            type="image/webp"
            sizes="calc(100vw - 4 * var(--page-padding)), (min-width:1024px) 621px"
          />
          <img
            src={fatherFallback}
            className={classes.illustration}
            width="100%"
            loading="lazy"
            alt="Чоловік тримає велосипед, на багажнику якого крісло з дитиною. Обоє в шоломах"
            ref={fatherRef}
          />
        </picture>
        <p className={classes.lead} ref={lead2Ref}>
          Розвиток велосипедного руху у Львівській <abbr>МТГ</abbr> сприяє
          розвитку сталої міської мобільності, соціально-економічному розвитку
          та покращенню екологічної ситуації на території громади. Створені
          комфортні умови для руху на велосипеді: безпечна та безбар’єрна
          велосипедна інфраструктура для пересування містом та іншими населеними
          пунктами громади забезпечує швидкий та безперервний рух, створена
          можливість безпечно зберігати свій велосипед протягом дня.
        </p>
      </div>
      <div className="container">
        <Pagination
          prev={{
            title: prevMeta.title,
            pathname: `/${prevMeta.slug}`,
          }}
          next={{
            title: nextMeta.title,
            pathname: `/${nextMeta.slug}`,
          }}
        />
      </div>
    </Layout>
  );
};

export const Head: HeadFC = () => (
  <PageHead title={title} themeColor="#1a1717" og={{ image: '' }} />
);

export default Page;
