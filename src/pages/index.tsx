import React from 'react';
import { HeadFC, PageProps, Link } from 'gatsby';
import config from '../../gatsby-config';
import { Layout } from '../components/Layout';
import { Intro } from '../components/Intro';
import { PageHead } from '../components/PageHead';
import * as classes from './homepage.module.scss';

const IndexPage: React.FC<PageProps> = () => {
  return (
    <Layout alternativeHeader>
      <Intro>
        <div className={[classes.intro, 'container'].join(' ')}>
          <h1 className={classes.title}>
            Програма розвитку велосипедного транспорту на території Львівської
            міської територіальної громади до 2033 року
          </h1>
          <div className={classes.actions}>
            <Link to="/zmist" className={classes.action}>
              Зміст
            </Link>
            <Link
              to="/vstup"
              className={[classes.action, classes.primary].join(' ')}
            >
              Читати
            </Link>
          </div>
        </div>
      </Intro>
    </Layout>
  );
};

export default IndexPage;

export const Head: HeadFC = () => (
  <PageHead
    themeColor="#1a1717"
    og={{ title: config.siteMetadata && (config.siteMetadata.title as string) }}
  />
);
